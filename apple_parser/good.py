# coding=utf-8

from apple_parser.url import URL
from bs4 import BeautifulSoup


class Good(object):
    def __init__(self, url):
        self.url = url

    # name, price = text.find_all('td')[1:]
    # self.name = name.find('a').text.strip()
    # self.price = re.findall(r'[0-9]+', price.find('span').text.replace(' ', ''))[0]
    # weight = re.findall(r'[0-9]+г', self.name)
    # self.weight = weight[0].replace('г', '') if weight else 1000

    def composition(self, soup):
        return soup.find('div', {'id': 't2'}).text

    def price(self, soup):
        return soup.find('strong', {'class': 'price'}).text

    def process(self):
        print("Обрабатываю страницу товара", self.url)
        content = URL(self.url).get()
        soup = BeautifulSoup(content, "html.parser")
        composition = self.composition(soup)
        price = self.price(soup)
        print('состав:', composition)
        print('цена:', price)


if __name__ == '__main__':
    _url = 'http://www.a-yabloko.ru/catalog/21/goods/28507/Sir_Gorgonzola_s_goluboy_plesenyu_50___ves_Braziliya'
    Good(_url).process()
