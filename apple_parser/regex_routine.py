# coding=utf-8

import re


def extract_all_numbers(text):
    return list(map(int, re.findall(r'[0-9]+', text)))
