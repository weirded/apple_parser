# coding=utf-8

from time import sleep
from apple_parser.apple import host
from apple_parser.good import Good
from apple_parser.url import URL
from bs4 import BeautifulSoup


class Page(object):
    """ Обработка страницы с товарами """

    def __init__(self, url):
        self.url = url

    def parse(self, content):
        soup = BeautifulSoup(content, "html.parser")
        for good in soup.select_one(".goods").find_all('tr'):
            url = host + good.find('a').attrs.get('href')
            Good(url).process()
            sleep(1)

    def process(self):
        print("Обрабатываю страницу", self.url)
        self.parse(URL(self.url).get())
