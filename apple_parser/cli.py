# coding=utf-8

import argparse
from apple_parser.category import Category


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--categories', nargs='+', type=int, help='Список категорий которые нужно отпарсить')
    parser.add_argument('-r', '--renew', help='Обновить данные в кэше', action='store_true', default=False)
    return parser.parse_args()


def main():
    args = parse_args()
    for category in args.categories:
        Category(category).iter_pages()


if __name__ == '__main__':
    main()
