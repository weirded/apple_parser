# coding=utf-8

from apple_parser.apple import host
from apple_parser.url import URL
from apple_parser.page import Page
from apple_parser.regex_routine import extract_all_numbers
from bs4 import BeautifulSoup


class Category(object):
    """ Логика работы с категорией товаров """
    url_template = host + '/catalog/{0}/'

    def __init__(self, category):
        self.index = 0
        self.url = self.url_template.format(category)
        self.soup = BeautifulSoup(URL(self.url).get(), 'html.parser')

    def page_count(self):
        return next(extract_all_numbers(item.text)[-1]
                    for item
                    in self.soup.find_all("div", {"class": 'sc-desktop'})
                    if 'всего' in item.text)

    def iter_pages(self):
        for index in range(1, self.page_count() + 1):
            Page('{0}page/{1}'.format(self.url, index)).process()
