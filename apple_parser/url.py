# coding=utf-8

import requests
from dictator import Dictator


class URL(object):
    """ Логика обращения к URL и кэширования """
    cache = '92.53.78.232'
    encoding = 'cp1251'

    def __init__(self, url, dictator=None):
        self.url = url
        self.dictator = dictator if dictator else Dictator(host=URL.cache)

    def __check_cache(self):
        return self.dictator.get(self.url)

    def update(self):
        """ Реальный запрос к серверу """
        print("Совершаем реальный запрос к", self.url)
        result = requests.get(self.url).content.decode(self.encoding)
        self.dictator.set(self.url, result)
        return result

    def get(self):
        """ Получение контента страницы из кэша или реальным запросом """
        return self.__check_cache() or self.update()
