from setuptools import setup

setup(
    name='apple_parser',
    version='0.0.1',
    packages=['apple_parser'],
    url='',
    license='MIT',
    author='Oleg Strizhechenko',
    author_email='oleg.strizhechenko@gmail.com',
    description='a-yabloko.ru parser.', install_requires=['beautifulsoup4']
)
